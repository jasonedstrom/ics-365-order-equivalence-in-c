//
//  main.c
//  Order Equivalence
//  This program tests order equivalance between 2 sequences of integers
//
//  Created by Jason Edstrom on 2/11/14.
//  Copyright (c) 2014 Jason Edstrom. All rights reserved.
//

#include <stdio.h>


/*
 *  reads a list of integers from the keyboard and
 *  parameter: list - the array
 */
int read_integers(int list[]);


/*
 *  splits the list array into 2 sequences and checks order equivalence
 *  parameter: list - the array
 *  parameter: total - total number of integers in the list
 */
int checkOrderEq(int total, int list[]);


int main(int argc, const char * argv[])
{
    int list[100];
    printf("Enter your sequence of number (separated by spaces): ");
    int size = read_integers(list);
    int result = checkOrderEq(size, list);
    
    if (result ==0){
        printf("This sequence is not order equivalent\n");
    } else if (result == 1){
        printf("This sequence is order equivalent\n");
    }
    
    return 0;
}

int read_integers(int list[])
{
    int i = 0;
    int num;
    
    while (scanf("%i",&num) > 0){
        
        if (num > 0){
        list[i] = num;
        i++;
        }
    }
    
    
    return i;
    
}

int checkOrderEq(int total, int list[]){
    
    int sequenceA [50];
    int sequenceB [50];
    
    if (total % 2){
        return 0;
    } else if( total == 2 || total == 0){
        return 1;
    } else {
        
        for(int i = 0; i < total/2; i++){
            sequenceA[i] = list[i];
        }
        
        for (int j = total/2; j < total; j++){
            sequenceB[j - (total/2)] = list[j];
        }
        
        printf("Sequences split\n");
        
        for (int k = 0; k < total/2; k++){
            for (int l = k; l < total/2; l++) {
                int valueA = 0;
                int valueB = 0;
                
                if (sequenceA[k] < sequenceA[l]){
                    valueA = -1;
                } else if (sequenceA[k] == sequenceA[l]){
                    valueA = 0;
                }else if (sequenceA[k] > sequenceA[l]){
                    valueA = 1;
                }
                
                if (sequenceB[k] < sequenceB[l]){
                    valueB = -1;
                } else if (sequenceB[k] == sequenceB[l]){
                    valueB = 0;
                }else if (sequenceB[k] > sequenceB[l]){
                    valueB = 1;
                }
                
                if (valueA != valueB){
                    return 0;
                }
                
            }
            
        }
        
        
        
    }
    
    return 1;
}

